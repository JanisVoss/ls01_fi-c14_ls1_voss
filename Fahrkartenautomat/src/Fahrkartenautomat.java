﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;

       System.out.print("Zu zahlender Betrag (EURO): ");
       zuZahlenderBetrag = fahrkartenbestellungErfassen();


       
       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = fahrkartenBezahlen(0.0, zuZahlenderBetrag);
     
       // Fahrscheinausgabe
       // -----------------
       fahrkartenAusgeben();
    

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
       
    }
    
    
    static double fahrkartenbestellungErfassen () 
    {
    Scanner tastaturzwei = new Scanner(System.in);
    double zuZahlenderBetrag = tastaturzwei.nextDouble();
    System.out.println("Bitte geben Sie ein, wieviele Tickets Sie kaufen möchten: ");
    int FahrkartenAnzahl = tastaturzwei.nextInt();
    System.out.println("Sie kaufen " + FahrkartenAnzahl + " Tickets beim Preis von " + zuZahlenderBetrag + " für einen Gesamtbetrag von " + (zuZahlenderBetrag * FahrkartenAnzahl) + " Euro");
    zuZahlenderBetrag = zuZahlenderBetrag * FahrkartenAnzahl;
    return (zuZahlenderBetrag);
    }
    
    
    static double fahrkartenBezahlen(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) 
    {
    	Scanner tastaturdrei = new Scanner(System.in);
    	double eingeworfeneMünze;
    	 while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
         {
      	   System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
      	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
      	   eingeworfeneMünze = tastaturdrei.nextDouble();
             eingezahlterGesamtbetrag += eingeworfeneMünze;
         }
    	 return (eingezahlterGesamtbetrag);
    }
    
    
    static void fahrkartenAusgeben()
    {
        System.out.println("\nFahrschein/e wird/werden ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    }
    
    
    static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
        double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if(rückgabebetrag > 0.0)
        {
     	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05;
            }
            
        }

        System.out.println("\nVergessen Sie nicht, den/die Fahrschein/e\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.");
     }

}
    
